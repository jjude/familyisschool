---
image: https://cdn.olai.in/jjude/kids-calendar-22.png
---

I do not see homeschooling as teaching school curriculums at home, rather as a way to train my children to earn a sustainable living. Neither quick money nor as much money, but sustainable wealth. My training is based on the [WINS framework](https://jjude.com/wins/). It goes as follows:

- **Wealth**: Define what wealth is. Absolute riches don't matter; what matters is to live life fully.
- **Insight**: Learning to learn is a fundamental skill. Seeing profitable, non-obvious ideas and holding on to them for a long time. It is important that they are able to research topics, synthesize ideas, present ideas, and organize a community around them.
- **Network**: They should create a diverse and dense network. Engaging conversations, selling their ideas, and building long-term relationships are skills they should be able to develop and display.
- **Self-Care & Control:** This is the foundation on which they can build their other three skills. They should have a healthy self-esteem, and they can only have a positive self-esteem if they take care of themselves and are in control of their lives.

All curriculum and activities will revolve around those four pillars.

I am starting with these:
- Stock market investing (value investing)
- Music
- YouTube video creation
- Art

## Value Investing

Stock market investing seems like an odd subject to teach to children 10 and 12. I see it as an introduction to a variety of topics. Learning value investing involves studying human psychology, anthropology, economics, pricing, and value, as well as different industries, finance, and most importantly, patience.

They have already made a few videos on these topics:

- [How to Value A stock](https://www.youtube.com/watch?v=uQuDd383xnE)
- [Should you Invest in Sonata?](https://www.youtube.com/watch?v=9jC30UWzitU)
- [Should I Invest In Graphite Limited](https://www.youtube.com/watch?v=EpcoQV6TmY4)
- [My Infosys Analysis!](https://www.youtube.com/watch?v=NPmGzqCeEXA)
- [My NMDC Analysis](https://www.youtube.com/watch?v=YrzAcBloa7I)
- [Coal India Analysis!](https://www.youtube.com/watch?v=nCipWHhp_Ww)

They are learning about researching, different industries, and how to speak confidently about their research. Even I am surprised at their growth.

After the age of 18, Indians are allowed to invest independently in stocks (till then, their income is included in the earnings of their parents). From now until the age of 18, I will teach them how to invest. I'll give them 5 lakhs from their education fund for them to make all the mistakes on the stock market for few years. After they have gained confidence, I will give them more money to invest and let them be on their own.

## Music

The elder one is learning guitar, while the younger one is learning cajon. They both like their respective lessons. They both have private tutors. I have requested their tutors to accelerate their learning (much like [what Kimo did for Derek Sivers](https://jjude.com/smartcuts/)).

## Industry Visits

Every quarter, they visit companies and factories run by my friends. I encourage them to talk to the founders. They have already visited three companies, and I am surprised by the questions they ask and the manner in which they behave with the founders.

Here are some of the videos they created out of their industry visits:

- [Their first industry visit - a tooling factory in the defense industry](https://www.youtube.com/watch?v=XfWVgThySbU)
- [Visit to The Furniture Factory](https://www.youtube.com/watch?v=uUkpTCpwxrQ)

## Weekly Schedule

![Homeschooling Weekly Schedule](https://cdn.olai.in/jjude/kids-calendar-22.png "Homeschooling Weekly Schedule")

In the mornings, they exercise three times a week and go for a walk with their mother in the nearby park two days a week. We always eat breakfast, lunch, and dinner together. We talk a lot at the dinner table. Our discussions cover their progress, any needed adjustments, stock market investing, philosophy, etc.

Then they go about their morning routines. They read five chapters from the Bible, write their understanding in two pages, and sometimes draw a visual out of what they read. Also, they must read the editorial in the newspaper and explain it in their own words. It serves as a practice for reading, understanding, and writing.

They read a lot of fiction. They read Geronimo Stilton, Shakespeare, Paulo Coelho, and like authors. In addition to increasing their imagination, these stories give them knowledge about history, psychology, and geography.

After lunch, they begin their productive time. They create videos about stock market analysis, unboxings of new purchases, historical figures, etc. Visit their YouTube channel to see what they produce.

They attend music class in the evening, two days a week. After that, their mother teaches them Tamil.

Twice a month, they spend a day in their friend's house. 

As you can see, there are lot of activities. The activities revolve around what they like, and in preparation for the trends that I anticipate to come in the future.

## Retros & Revisions

This will, of course, evolve as I learn and progress. We review each week / month / quarter and revise as needed. 