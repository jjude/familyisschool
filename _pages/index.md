---
layout: page
title: Home
id: home
permalink: /
---

# Welcome! 🌱

![Family Is School](https://cdn.olai.in/fis/fis-web-pg.jpg "Family Is School")

I'm Joseph Jude, a homeschooling dad. I started homeschooling my two boys since October 2021. When people found out about our homeschooling, they were curious and asked mostly similar questions. So I created this site to share our journey.

## What You'll Find Here

- [[Why we started homeschooling]]
- [[How we are homeschooling]]
- [[Frequently asked questions]]
- [[Resources]]

[Tweet](https://twitter.com/familyisschool) 👋 to me your feedback and questions. When you add the url of the page you are talking about, it helps me to relate your comment to that page.

<style>
  .wrapper {
    max-width: 46em;
  }
</style>
