class ReplaceMediaFolder < Jekyll::Generator
    def generate(site)
        all_notes = site.collections['notes'].docs
        all_notes.each do |current_note|
            current_note.content.gsub!(
                "_media/","../assets/"
            )
        end
    end
end