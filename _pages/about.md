---
layout: page
title: About
permalink: /about
---

I'm Joseph Jude, a homeschooling dad. I started homeschooling my two boys since October 2021. Here in these pages, you'll find our journey of homeschooling.

[Tweet](https://twitter.com/familyisschool) 👋 to me your feedback and questions.