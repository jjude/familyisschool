---
image: https://cdn.olai.in/fis/fis-web-pg.jpg
---

#### Our Resources

Here are the resources from which we are sharing what we learn and know. These are our first-hand experiences.

If you have any questions, please respond at one of these social media handles and will respond to the queries.

- [Website](https://familyisschool.com)
- [Twitter](https://twitter.com/familyisschool)
- [Facebook](https://www.facebook.com/familyisschool)
- [YouTube](https://www.youtube.com/garrettsboys)

#### Resources we have used
- [Yellow Class](https://www.yellowclass.com/) - Both boys attended few sessions here. They are fun. 
- [Khan Academy](https://www.khanacademy.org/) - The original online school 😜
- [British Library](https://www.britishcouncil.in/library) - Before Covid struck, we used to be members of BL. A delight for book lovers. They had wide variety of books and cheap too.
- [The Browser Library](https://www.thebrowser.org/) - We have started to use this library in Chandigarh recently. Seems to be decent.

#### Other Resources
- [National Insitute of Open Schooling](https://www.nios.ac.in/) - If you want to follow a curriculum or get back to educational stream after homeschooling or a break.
- [CK12](http://ck12.org/student)
- [Open Stax](http://openstax.org)
- [CommonLit](http://commonlit.org)
- [Kids Britannica](http://kids.britannica.com/students)
- [English Language Teaching At Home](http://elt.oup.com/feature/global/learnathome/parents)
- [Oxford OWL](http://home.oxfordowl.co.uk)
- [Interactive Simulations For Science & Math](http://phet.colorado.edu)
- [WolframAlpha](http://wolframalpha.com)