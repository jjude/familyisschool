---

---

## How will they get into college?

I do not plan to get them into college. They'll continue to do homeschooling and be able to earn a living on their own. In my prediction, they may not go in as employees but as job creators; or even if they are employees, they will begin earning on their own before they turn 21 years old, which is the usual age at which Indians enter the workforce.

If something were to happen to me, there are options for them to write exams to get back into regular educational streams. Refer the podcast I did with [Mahendran](https://jjude.com/mahendrank/) for those options.

## What about social skills?

It is called homeschooling. Not cave-schooling. 

Kids are naturally attracted to talking and making friends. You have to force them to keep quiet. So unless you hide in isolation, no, they don't miss out on social skills. My kids mingle with other kids of the same age in the family; they move with kids in church; they also get to know kids of my colleagues. When time comes, I will send them to sports coaching, where they will meet people and move with them. All along, they also mingle with people of other age.

Another fact I observed. They don't hesitate to talk to CEOs when take them on industry tours, unlike the school going kids who are conditioned not to talk to elders.

They also are freely talking to kids of all age and classes (upper class, lower class) since I take them everywhere. In school they are exposed only to one set of people.

## How will you know if they are learning?

In homeschooling, there are no tests. They won't compete with others. We set certain goals every quarter and they work daily to achieve those goals. I train them to create a vision for the future, plan how to realize that vision, and organize their days accordingly. I'm happy as long as they improve every quarter. Plus, they will be able to prove their skills by being paid for their work or making money by investing in the stock market.

## Should I homeschool my kids?

I can't answer for you. Everyone's situation is different. I'm freely sharing my thinking behind this experiment and how I'm going about it. I'm open to talk to you in detail, if you are really interested. You can get my contact details from [[contact]] page.
