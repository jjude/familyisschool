---
title: Contact
---

I'm active on Twitter & LinkedIn. I'll respond as quickly as possible when you contact me on these channels. I might be on other channels too, but may not be as active as I'm on Twitter and LI.

-   [Twitter](https://twitter.com/familyisschool)
-   [LinkedIn](https://www.linkedin.com/in/jjude/)